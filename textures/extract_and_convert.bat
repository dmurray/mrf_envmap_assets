@echo off

REM Script to extract and convert textures from TextureHaven.

for %%a in (*.zip) do (
  7z x  %%a -o%%~na
  del %%a
)

pause

for /R %%a in (*_albedo*.png) do (
  cd %%~pa

  echo Processing %%a
  start "Convert" /wait rgb2spec -nbw 16 -in_file %%a -out "%%~na_SPEC16.hdr"
  
  cd %~dp0
)
for /R %%a in (*_diff*.png) do (
  cd %%~pa

  echo Processing %%a
  start "Convert" /wait rgb2spec -nbw 16 -in_file %%a -out "%%~na_SPEC16.hdr"
  
  cd %~dp0
)
for /R %%a in (*_spec*.png) do (
  cd %%~pa

  echo Processing %%a
  start "Convert" /wait rgb2spec -nbw 16 -in_file %%a -out "%%~na_SPEC16.hdr"
  
  cd %~dp0
)

pause