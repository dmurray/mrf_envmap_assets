Repository to host spectral assets
================================

Contains environment maps:
- From HDRiHaven (https://hdrihaven.com/)
- From Hosek and Wilkie model (REF) in the Skydome subfolder.

Also contains textures for material from TextureHaven (https://texturehaven.com/).

All are color assets are available as ENVI spectral files (.hdr header and .raw data files).

These assets may be use in combination with mesh and IOR in https://gitlab.inria.fr/adufay/mrf_assets.
