HDRI RGB Envmaps (single .hdr files) in this folder are from:
https://hdrihaven.com
Under CC0 license (https://hdrihaven.com/p/license.php).

Spectral version (combination of lightweight .hdr and heavy .raw files) are conversion obtained with the rgb2sepc utility from the Malia Renderinf Framework (https://gitlab.inria.fr/pacanows/MRF / https://pacanows.gitlabpages.inria.fr/MRF/apps/malia/main.md.html).
